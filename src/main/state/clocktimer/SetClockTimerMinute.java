package main.state.clocktimer;

import java.awt.Color;

import main.component.*;
import main.state.compositestate.clocktimer.ClockTimerSettingState;

public class SetClockTimerMinute extends ClockTimerSettingState {
	
	// use Singleton design pattern
	private static SetClockTimerMinute instance;
	private SetClockTimerMinute() { // make default constructor private
		BUTTON_ONE_NAME = "NEXT";
		BUTTON_TWO_NAME = "+";
		BUTTON_THREE_NAME = "-";
	}
	public static SetClockTimerMinute Instance() {
		if (instance==null) {
			instance = new SetClockTimerMinute(); }
		return instance; }
	
	public void button1Pressed(ClockSystem context) {
		if (context.hasClock()) {
			context.getClock().setMinuteColor(Color.BLACK);
			context.getClock().setSecondColor(new Color(46,127, 189));}
		context.setState(SetClockTimerSecond.Instance()); }

	public void button2Pressed(ClockSystem context) {
		context.getClockTimer().getTime().increaseMinute();
		context.notifyClock(); }

	public void button3Pressed(ClockSystem context) {
		context.getClockTimer().getTime().decreaseMinute();
		context.notifyClock(); }

 }