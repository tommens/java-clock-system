package main.state.clocktimer;

import java.awt.Color;

import main.component.*;
import main.state.compositestate.clocktimer.ClockTimerSettingState;

public class SetClockTimerSecond extends ClockTimerSettingState {
	
	// use Singleton design pattern
	private static SetClockTimerSecond instance;
	private SetClockTimerSecond() { // make default constructor private
		BUTTON_ONE_NAME = "CONFIRM";
		BUTTON_TWO_NAME = "+";
		BUTTON_THREE_NAME = "-";
	}
	public static SetClockTimerSecond Instance() {
		if (instance==null) {
			instance = new SetClockTimerSecond(); }
		return instance; }
	
	public void button1Pressed(ClockSystem context) {
		if (context.hasClock()) {
			context.getClock().setSecondColor(Color.BLACK); }
		context.setState(DisplayClockTimerOff.Instance()); }

	public void button2Pressed(ClockSystem context) {
		context.getClockTimer().getTime().increaseSecond();
		context.notifyClock(); }

	public void button3Pressed(ClockSystem context) {
		context.getClockTimer().getTime().decreaseSecond();
		context.notifyClock(); }

 }