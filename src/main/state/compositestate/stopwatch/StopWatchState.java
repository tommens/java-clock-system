package main.state.compositestate.stopwatch;

import main.component.ClockSystem;
import main.component.Mode;
import main.state.compositestate.ClockState;
import main.utility.Time;

abstract public class StopWatchState extends ClockState {
	
	public Mode getMode() { return Mode.STOPWATCH; }

	public Time getObservedTime(ClockSystem context) {
		return context.getStopWatch().getTime(); }

}
