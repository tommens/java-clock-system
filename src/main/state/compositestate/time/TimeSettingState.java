package main.state.compositestate.time;

import main.component.ClockSystem;
import main.utility.Time;

abstract public class TimeSettingState extends TimeState {

	public Time getObservedTime(ClockSystem context) {
		return context.getDisplayState().getObservedTime(context); } 
	
	public String getIcon(ClockSystem context) {
		return context.getDisplayState().getIcon(context); }
}
