package main.state.compositestate.time;

import main.component.ClockSystem;
import main.component.Mode;
import main.state.compositestate.ClockState;
import main.utility.Time;

abstract public class TimeState extends ClockState {
	
	public Mode getMode() { return Mode.WATCH; }

	abstract public Time getObservedTime(ClockSystem context);
	
	}