package main.state.compositestate.clocktimer;

import main.component.ClockSystem;
import main.component.Mode;
import main.state.compositestate.ClockState;
import main.utility.Time;

abstract public class ClockTimerState extends ClockState {
		
	public Mode getMode() { return Mode.CLOCKTIMER; }

	public Time getObservedTime(ClockSystem context) {
		return context.getClockTimer().getTime();
		}

	}