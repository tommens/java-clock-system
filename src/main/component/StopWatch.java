package main.component;

import main.state.compositestate.stopwatch.StopWatchDisplayState;
import main.state.stopwatch.DisplayStopWatchOff;
import main.state.stopwatch.DisplayStopWatchOn;
import main.utility.*;

public class StopWatch implements TimeOwner {
	
	private ClockSystem clockSystem;
	private Time time;
	private StopWatchDisplayState powerState;
	private MemoryTimeArray memoryTime;
	private TimeTimer stopWatchTimer;
	
	public StopWatch() {
		powerState = DisplayStopWatchOff.Instance();
		time = new Time();
		memoryTime = new MemoryTimeArray(3); }
	
	public void setClockSystem(ClockSystem clockSystem) {
		this.clockSystem = clockSystem; }
	
	public void notifyClockSystem() {
		if (clockSystem != null) {
			clockSystem.notifyClock(); } }

	public Time getTime() {
		return time; }

	public StopWatchDisplayState getPowerState() {
		return powerState; }

	public MemoryTimeArray getMemoryTime() {
		return memoryTime; }

	public TimeTimer getStopWatchTimer() {
		return stopWatchTimer; }

	public void addMemoryTime() {
		memoryTime.add(new Time(time.getHour(), time.getMinute(), time.getSecond()));
		notifyClockSystem(); }

	public void reset() {
		time = new Time();
		memoryTime.clear();
		notifyClockSystem(); }

	public void tick() {
		time.tickUp();
		notifyClockSystem(); }
		
	public void start() {
		powerState = DisplayStopWatchOn.Instance();
		stopWatchTimer = new TimeTimer(this);
		stopWatchTimer.start(); }
		
	public void stop() {
		powerState = DisplayStopWatchOff.Instance();
		stopWatchTimer.cancel(); } }
