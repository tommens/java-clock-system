package main.component;

import main.state.alarm.TriggeredAlarm;
import main.state.clocktimer.DisplayClockTimerOff;
import main.state.clocktimer.DisplayClockTimerOn;
import main.state.compositestate.clocktimer.ClockTimerDisplayState;
import main.utility.*;

public class ClockTimer implements TimeOwner {
	
	private ClockSystem clockSystem;
	private Time time;
	private ClockTimerDisplayState powerState;
	private TimeTimer clockTimerTimer;
	
	public ClockTimer() {
		time = new Time();
		powerState = DisplayClockTimerOff.Instance(); }

	public Time getTime() {
		return time; }
	
	public void setClockSystem(ClockSystem clock) {
		clockSystem = clock; }
	
	public void notifyClockSystem() {
		if (clockSystem != null) clockSystem.notifyClock(); }
	
	public void notifyClockTimerIsOff() {
		if (clockSystem!=null &&
				clockSystem.getState()==DisplayClockTimerOn.Instance()) {
			clockSystem.setState(powerState); }
		if (clockSystem!=null &&
				clockSystem.getState()==TriggeredAlarm.Instance() && 
				TriggeredAlarm.getMemoryState()==DisplayClockTimerOn.Instance()) {
			TriggeredAlarm.setMemoryState(powerState); } }

	public ClockTimerDisplayState getPowerState() {
		return powerState; }

	public TimeTimer getClockTimerTimer() {
		return clockTimerTimer; }

	public void ring() {
		Audio mySound = new Audio("/resources/sound/bipbip.wav");
		mySound.start(); }

	public void tick() {
		time.tickDown();
		if (time.isOne()) {
			time.tickDown();
			ring();
			stop();
			notifyClockTimerIsOff(); }
		notifyClockSystem(); }
		
	public void stop() {
		powerState = DisplayClockTimerOff.Instance();
		clockTimerTimer.cancel(); }
		
	public void start() {
		if (!time.isZero()) {
			powerState = DisplayClockTimerOn.Instance();
			clockTimerTimer = new TimeTimer(this);
			clockTimerTimer.start(); } } }
