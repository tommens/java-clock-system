package main;

import java.awt.FontFormatException;
import java.io.IOException;

import main.GUI.ClockFrame;

public class Launcher {
	
	public static void main(String[] args) throws FontFormatException, IOException {

		ClockFrame clock = new ClockFrame();
		clock.setVisible(true); }
	}