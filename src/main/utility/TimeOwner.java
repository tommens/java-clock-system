package main.utility;


public interface TimeOwner {
	
	public Time getTime(); 
	public void tick(); }
