package test.immediate.component;

import static org.junit.Assert.assertTrue;
import main.component.ClockTimer;

import org.junit.Before;
import org.junit.Test;

public class ClockTimerTest {
	
	private ClockTimer ct;

	@Before
	public void setUp() {
		ct = new ClockTimer(); }
	
	@Test
	public void startInZeroTest() {
		ct.start();
		assertTrue(ct.getTime().isZero()); } }
