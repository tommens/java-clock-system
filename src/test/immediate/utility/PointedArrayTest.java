package test.immediate.utility;

import static org.junit.Assert.*;
import main.utility.PointedArray;

import org.junit.Before;
import org.junit.Test;

public class PointedArrayTest {
	
	private PointedArray<Integer> array;

	@Before
	public void setUp() {
		Integer[] intArray = {new Integer(0), new Integer(1), new Integer(2)};
		array = new PointedArray<Integer>(intArray); }
	
	@Test (expected=IndexOutOfBoundsException.class)
	public void setPointerException1() {
		array.setPointer(-1); }

	@Test (expected=IndexOutOfBoundsException.class)
	public void setPointerException2() {
		array.setPointer(3); }

	@Test (expected=IndexOutOfBoundsException.class)
	public void getException1() {
		array.get(-1); }

	@Test (expected=IndexOutOfBoundsException.class)
	public void getException2() {
		array.get(3); }

	@Test
	public void getPointedItemTest() {
		array.setPointer(2);
		assertEquals(new Integer(2), array.getPointedItem());
		array.setPointer(1);
		assertEquals(new Integer(1), array.getPointedItem());
		}
	
}
