package test.immediate.utility;

import static org.junit.Assert.*;
import main.utility.MemoryTimeArray;
import main.utility.Time;

import org.junit.Before;
import org.junit.Test;

public class MemoryTimeArrayTest {
	
	private MemoryTimeArray memo;

	@Before
	public void setUp() {
		memo = new MemoryTimeArray(3);
		Time timeOne = new Time(1,0,0);
		Time timeTwo = new Time(0,1,0);
		Time timeThree = new Time(0,0,1);
		memo.add(timeOne);
		memo.add(timeTwo);
		memo.add(timeThree); }
	
	@Test
	public void addTest() {
		Time expected = memo.get(1);
		Time added = new Time(11,11,11);
		memo.add(added);
		assertTrue(memo.get(0).equals(expected) && memo.get(2).equals(added)); } }
