package test.notimmediate;

import static org.junit.Assert.*;
import main.component.ClockTimer;
import main.state.clocktimer.DisplayClockTimerOff;
import main.state.clocktimer.DisplayClockTimerOn;
import main.utility.Time;

import org.junit.Before;
import org.junit.Test;

public class ClockTimerTest {
	
	private ClockTimer ct;

	@Before
	public void setUp() {
		ct = new ClockTimer(); }
	
	@Test
	public void autoStopStateTest() throws InterruptedException {
		ct.getTime().setSecond(2);
		ct.start();
		assertSame(DisplayClockTimerOn.Instance(), ct.getPowerState());
		
		Thread.sleep(2100);
		assertSame(DisplayClockTimerOff.Instance(), ct.getPowerState());
		}
	
	@Test
	public void tickTest() throws InterruptedException {
		ct.getTime().setMinute(1);
		ct.start();
		Thread.sleep(2100);
		ct.stop();
		Time expected = new Time(0,0,58);
		assertTrue(ct.getTime().equals(expected));
		}
	}
