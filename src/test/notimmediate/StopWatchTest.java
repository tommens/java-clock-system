package test.notimmediate;

import static org.junit.Assert.*;
import main.component.StopWatch;
import main.utility.Time;

import org.junit.Before;
import org.junit.Test;

public class StopWatchTest {
	
	private StopWatch sw;

	@Before
	public void setUp() {
		sw = new StopWatch(); }
	
	@Test
	public void tickTest() throws InterruptedException {
		sw.start();
		Thread.sleep(2100);
		sw.stop();
		assertTrue(sw.getTime().equals(new Time(0,0,2)));
		}
	
	@Test
	public void resetingTest() throws InterruptedException {
		sw.start();
		Thread.sleep(2000);
		sw.stop();
		Time previous = sw.getTime();
		sw.reset();
		assertFalse(previous.isZero());
		Time actual = sw.getTime();
		assertTrue(actual.isZero());
		}
	}
